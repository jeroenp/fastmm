# FastMM synced clone from SVN at SF.net

This is a FastMM synced clone from SVN repository at <http://fastmm.sourceforge.net>

This makes it easier to sync your repositories when using only DVCS tools like [SourceTree](http://www.sourcetreeapp.com/).

I will monitor changes from <http://sourceforge.net/p/fastmm/code/HEAD/tree/> and sync them shortly after they occur.

FastMM has an excellent [README](https://bitbucket.org/jeroenp/fastmm/src/git-svn/FastMM4_Readme.txt) and [FAQ](https://bitbucket.org/jeroenp/fastmm/src/git-svn/FastMM4_FAQ.txt).

--jeroen


##### Notes to self

###### Getting the latest SVN changes:

    git svn rebase

###### Initial repository creation and add to Bitbucket (or GitHub)

    git svn clone http://svn.code.sf.net/p/fastmm/code/ FastMM.git

Notes (see <http://viget.com/extend/effectively-using-git-with-subversion> and <http://www.janosgyerik.com/practical-tips-for-using-git-with-large-subversion-repositories/> for explanation): 

1. Do not include the `-s` option after `git svn clone`, as this SVN repository does not have the default `trunk`/`branches`/`tags` structure.
2. There are no [SVN ignore](http://stackoverflow.com/questions/17298668/svn-ignore-like-gitignore) entries in this repository, so this is not needed for `git svn show-ignore > .gitignore` 

Add the repository to Bitbucket or GitHub:

- <http://www.therealtimeweb.com/index.cfm/2012/5/16/move-git-repo-to-bitbucket>
- <https://help.github.com/articles/adding-an-existing-project-to-github-using-the-command-line>

Add either of these URLs to [Feedly](http://feedly.com) for monitoring: 

- <http://sourceforge.net/p/fastmm/code/HEAD/tree/>
- <http://sourceforge.net/p/fastmm/code/feed>